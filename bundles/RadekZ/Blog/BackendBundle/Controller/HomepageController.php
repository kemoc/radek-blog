<?php
declare(strict_types=1);

namespace RadekZ\Blog\BackendBundle\Controller;

use RadekZ\Blog\BackendBundle\Repository\PostRepository;
use RadekZ\Blog\BackendBundle\Repository\TagRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use RadekZ\Blog\BackendBundle\Controller\BlogAbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session;
use RadekZ\Blog\BackendBundle\Events;
use DateTime;

/**
 * Class HomepageController
 *
 * @Route("/{locale}",
 *      requirements={
 *          "locale"="%app_locales%",
 *      },
 *      defaults={
 *          "locale"="%locale%",
 *      }
 * )
 *
 * @package RadekZ\Blog\BackendBundle\Controller\HomepageController
 */
class HomepageController extends BlogAbstractController
{
    /**
     * @Route("/{page}/{format}",
     *      requirements={
     *          "page"="\d+",
     *          "format"="html|xml"
     *      },
     *      defaults={
     *          "page": "1",
     *          "format"="html",
     *          "locale"="%locale%",
     *          "templateName"="@blog_frontend/full/homepage.html.twig"
     *      },
     *      methods={"GET"},
     *      name="homepage"
     * )
     * @Cache(smaxage="30")
     *
     * NOTE: For standard formats, Symfony will also automatically choose the best
     * Content-Type header for the response.
     * See https://symfony.com/doc/current/quick_tour/the_controller.html#using-formats
     */
    public function indexAction(Request $request, Session $session, PostRepository $postsRepo,
                                TagRepository $tagsRepo,
                                EventDispatcherInterface $eventDispatcher,
                                string $locale,
                                int $page,
                                string $format,
                                string $templateName
    ): Response
    {
        $tag = null;
        if ($request->query->has('tag')) {
            $tag = $tagsRepo->findOneBy(['name' => $request->query->get('tag')]);
            if($tag) {
                $event = new GenericEvent($tag);
                $eventDispatcher->dispatch(Events::POSTS_SEARCHED_BY_TAG_BEFORE, $event);
            }
        }
        if($page < 1) {
            $page = 1;
        }
        $latestPosts = [];
        $latestPosts = $postsRepo->findLatest($page, $tag);

        $postsMostPopular = [];
        $postsMostPopular = $postsRepo->findMostPopular(1);
        $postsMostPopularInMonth = [];
        $now = new DateTime();
        $postsMostPopularInMonth = $postsRepo->findMostPopularInMonth($now, 1);

        // Every template name also has two extensions that specify the format and
        // engine for that template.
        // See https://symfony.com/doc/current/templating.html#template-suffix
        $templateName = str_replace('.html.', ".$format.", $templateName);
        $response = $this->render(
            $templateName,
            [
                'posts' => $latestPosts,
                'postsMostPopular' => $postsMostPopular,
                'postsMostPopularInMonth' => $postsMostPopularInMonth,
            ]
        );

        return $this->action($response);
    }
}

