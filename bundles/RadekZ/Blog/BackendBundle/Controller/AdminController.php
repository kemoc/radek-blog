<?php
declare(strict_types=1);


namespace RadekZ\Blog\BackendBundle\Controller;

use RadekZ\Blog\BackendBundle\Repository\PostRepository;
use RadekZ\Blog\BackendBundle\Repository\TagRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use RadekZ\Blog\BackendBundle\Controller\BlogAbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class AdminController
 * @Route("/admin")
 *
 * @package RadekZ\Blog\BackendBundle
 */
class AdminController extends BlogAbstractController
{


}

