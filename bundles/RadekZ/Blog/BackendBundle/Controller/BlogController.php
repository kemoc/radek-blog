<?php
declare(strict_types=1);


namespace RadekZ\Blog\BackendBundle\Controller;

use RadekZ\Blog\BackendBundle\Controller\BlogAbstractController;
use RadekZ\Blog\BackendBundle\Entity\Account;
use RadekZ\Blog\BackendBundle\Entity\Comment;
use RadekZ\Blog\BackendBundle\Entity\Post;
use RadekZ\Blog\BackendBundle\Events;
use RadekZ\Blog\BackendBundle\Form\CommentType;
use RadekZ\Blog\BackendBundle\Repository\PostRepository;
use RadekZ\Blog\BackendBundle\Repository\TagRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use DateTime;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * Controller used to manage blog contents in the public part of the site.
 *
 * @Route("/blog")
 * @Security("has_role('ROLE_BLOGGER')")
 *
 * @author Radek Z
 */
class BlogController extends BlogAbstractController
{
    public const DESIGN_NAME = '@blog_frontend';

    /**
     * @Route("/",
     *     defaults={
     *          "page": "1",
     *          "_format"="html",
     *          "templateName"="@blog_frontend/full/blog/blog.html.twig"
     *      },
     *     requirements={
     *          "page"="[1-9]\d*",
     *          "_format"="html|xml"*
     *      },
     *     methods={"GET"},
     *     name="blog_index"
     * )
     * @Route("/rss.xml",
     *     defaults={
     *          "page": "1",
     *          "_format"="xml",
     *          "templateName"="@blog_frontend/full/blog/blog.xml.twig"
     *      },
     *     requirements={
     *          "page"="[1-9]\d*",
     *          "_format"="xml"*
     *      },
     *     methods={"GET"},
     *     name="blog_rss"
     * )
     * @Route("/page/{page<[1-9]\d*>}", defaults={"_format"="html"}, methods={"GET"}, name="blog_index_paginated")
     * @Cache(maxage="10")
     *
     * NOTE: For standard formats, Symfony will also automatically choose the best
     * Content-Type header for the response.
     * See https://symfony.com/doc/current/quick_tour/the_controller.html#using-formats
     */
    public function index(Request $request,
                          PostRepository $postsRepo,
                          TagRepository $tagsRepo,
                          TokenStorageInterface $tokenStorage,
                          EventDispatcherInterface $eventDispatcher,
                          int $page = 1,
                          string $_format = 'html',
                          string $templateName = self::DESIGN_NAME . '/full/blog/blog.html.twig'
    ): Response
    {
        $tag = null;
        if ($request->query->has('tag')) {
            $tag = $tagsRepo->findOneBy(['name' => $request->query->get('tag')]);
        }
        if($page < 1) {
            $page = 1;
        }
        $token = $tokenStorage->getToken();
        /** @var Account $account */
        $account = $token->getUser();

        $postsMostPopular = [];
        $postsMostPopular = $postsRepo->findMostPopularByAuthor($account, 1);
        $postsMostPopularInMonth = [];
        $now = new DateTime();
        $postsMostPopularInMonth = $postsRepo->findMostPopularInMonthByAuthor($account, $now, 1);

        $postsLatestInMonth = $postsRepo->findLatestByAuthorInMonth($account, $now);

        $latestPosts = [];
        $latestPosts = $postsRepo->findLatestByAuthor($account, $page);
        //count($latestPosts);
        //var_dump($latestPosts);
        // Every template name also has two extensions that specify the format and
        // engine for that template.
        // See https://symfony.com/doc/current/templating.html#template-suffix
        $templateName = str_replace('.html.', ".$_format.", $templateName);
        $response = $this->render(
            $templateName,
            [
                'postsUnpublished' => $postsRepo->findLatestUnpublished(1, null, 50),
                'postsMostPopularInMonth' => $postsMostPopularInMonth,
                'postsMostPopular' => $postsMostPopular,
                'postsLatestInMonth' => $postsLatestInMonth,
                'posts' => $latestPosts,
            ]
        );

        return $this->action($response);
    }
}
