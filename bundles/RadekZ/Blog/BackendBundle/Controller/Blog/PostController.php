<?php
declare(strict_types=1);


namespace RadekZ\Blog\BackendBundle\Controller\Blog;

use Psr\Log\LoggerInterface;
use RadekZ\Blog\BackendBundle\Entity\Account;
use RadekZ\Blog\BackendBundle\Entity\Comment;
use RadekZ\Blog\BackendBundle\Entity\Post;
use RadekZ\Blog\BackendBundle\Entity\Tag;
use RadekZ\Blog\BackendBundle\EventData;
use RadekZ\Blog\BackendBundle\EventDispatcher\PostCreateBeforeEvent;
use RadekZ\Blog\BackendBundle\EventDispatcher\PostCreatedEvent;
use RadekZ\Blog\BackendBundle\EventDispatcher\PostCreateUpdateEvent;
use RadekZ\Blog\BackendBundle\EventDispatcher\PostUpdateBeforeEvent;
use RadekZ\Blog\BackendBundle\EventDispatcher\PostUpdatedEvent;
use RadekZ\Blog\BackendBundle\Events;
use RadekZ\Blog\BackendBundle\Form\CommentType;
use RadekZ\Blog\BackendBundle\Form\Data\Post\Create;
use RadekZ\Blog\BackendBundle\Form\Data\Post\Update;
use RadekZ\Blog\BackendBundle\Form\Factory\Post\FormFactory;
use RadekZ\Blog\BackendBundle\Form\Type\Post\CreateType;
use RadekZ\Blog\BackendBundle\Repository\PostRepository;
use RadekZ\Blog\BackendBundle\Repository\TagRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Form\ClickableInterface;
use Cocur\Slugify\Slugify;
use RadekZ\Blog\BackendBundle\EventDispatcher\PostReadEvent;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Controller used to manage blog contents in the public part of the site.
 *
 * @Route("/blog/post")
 *
 * @author Radek Zadroga
 */
class PostController extends AbstractController
{
    public const DESIGN_NAME = '@blog_frontend';

    /**
     * @var FormFactory
     */
    protected $formFactory;

    public function __construct(FormFactory $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    /**
     * @Route("/create",
     *     methods={"GET", "POST"},
     *     name="blog_post_create",
     *     defaults={
     *          "templateName"="@blog_frontend/full/blog/post/create.html.twig"
     *      }
     * )
     * @Security("has_role('ROLE_BLOGGER')")
     *
     */
    public function create(Request $request, EventDispatcherInterface $eventDispatcher, LoggerInterface $logger,
                           TokenStorageInterface $tokenStorage,
                           TranslatorInterface $translator,
                           string $templateName = "@blog_frontend/full/blog/post/create.html.twig"
    ): Response
    {
        $params = [];
        /** @var Account $account */
        $account = $tokenStorage->getToken()->getUser();

        $post = new Post();
        $post->setAuthor($account);
        $post->setAuthorNickname($account->getNickname());
        $post->setModifier($account);
        $post->setModifierNickName($account->getNickname());

        $postCreateData = new Create($post, $translator);
        $form = $this->formFactory->getCreate($postCreateData);

        $form->handleRequest($request);
        if($form->isSubmitted()) {

            if(!$form->isValid()) {

            }
            else {
                $doctrine = $this->getDoctrine();
                /** @var Create $postCreateData */
                $postCreateData = $form->getData();
                $post = $postCreateData->getPostEntity();
                $postCreateData->copyTo($post);

                $event = new PostCreateBeforeEvent($post);
                $event->setForm($form);
                $eventDispatcher->dispatch(Events::POST_CREATE_BEFORE, $event);
                $isPublished = $event->isActionPublished();

                $em = $doctrine->getManager();
                $em->persist($post);
                $em->flush();

                /** @var TagRepository $tagRepo */
                $tagRepo = $doctrine->getRepository(Tag::class);
                $tags = [];
                foreach(Post::tagsStringToArray($post->getTags2()) as $itemKey => $tagNameItem) {
                    $tagX = $tagRepo->findOneByName($tagNameItem);
                    if(!$tagX) {
                        $tagX = new Tag();
                        $tagX->setName($tagNameItem);
                    }
                    $tags[] = $tagX;
                }
                if($tags) {
                    $post->setTags($tags);
                    $em->persist($post);
                    $em->flush();
                }
                $postSlug = $post->getSlug();

                $routeData = new EventData\RedirectToRoute();
                if($isPublished) {
                    $routeData->setRouteName('blog_post');
                    $routeData->setParameters(['slug' => $postSlug]);

                    //return $this->redirectToRoute('blog_post', ['slug' => $postSlug]);
                }
                else {
                    $routeData->setRouteName('blog_post_draft');
                    $routeData->setParameters(['slug' => $postSlug]);
                }
                $event = new PostCreatedEvent($post);
                $event->setRouteData($routeData);
                $event->setIsActionPublished($isPublished);
                $eventDispatcher->dispatch(Events::POST_CREATED, $event);

                return $this->redirectToRoute($routeData->getRouteName(), $routeData->getParameters());
            }
        }
        $dataCollection = new ArrayCollection();
        $params['dataCollection'] = $dataCollection;

        $args = $params;
        $args['form'] = $form;

        $response = new Response();
        $args['response'] = $response;

        $event = new PostCreateUpdateEvent($post, $args);
        $eventDispatcher->dispatch(Events::POST_CREATE_RENDER_BEFORE, $event);

        $params['formView'] = $form->createView();
        $params['post'] = $post;

        $params = $params + (array)$dataCollection;

        return $this->render($templateName, $params, $response);
    }

    /**
     * @see https://github.com/cocur/slugify
     *
     * @Route("/update/{slug}",
     *     methods={"GET", "POST"},
     *     name="blog_post_update",
     *     defaults={
     *          "templateName"="@blog_frontend/full/blog/post/update.html.twig"
     *      }
     * )
     * @Security("has_role('ROLE_BLOGGER')")
     * @Entity("post", expr="repository.findOnePublishedBySlug(slug)")
     *
     */
    public function update(Request $request, EventDispatcherInterface $eventDispatcher,
                           LoggerInterface $logger,
                           TokenStorageInterface $tokenStorage,
                           TranslatorInterface $translator,
                           string $slug,
                           string $templateName = "@blog_frontend/full/blog/post/update.html.twig"
    )
    {
        $doctrine = $this->getDoctrine();

        /** @var Account $account */
        $account = $tokenStorage->getToken()->getUser();

        /** @var PostRepository $postRepo */
        $postRepo = $doctrine->getRepository(Post::class);
        $post = $postRepo->findOneBySlugAndAuthor($slug, $account);
        if(!$post) {
            throw $this->createNotFoundException("Not found your post.");
        }

        $oldPostTitle = $post->getTitle();
        $params = [];

        $postUpdateData = new Update($post, $translator);
        $form = $this->formFactory->getUpdate($postUpdateData);

        $form->handleRequest($request);
        if($form->isSubmitted()) {

            if(!$form->isValid()) {

            }
            else {
                /** @var Update $postUpdateData */
                $postUpdateData = $form->getData();
                $post = $postUpdateData->getPostEntity();
                $postUpdateData->copyTo($post);

                $now = new \DateTime();

                $event = new PostUpdateBeforeEvent($post);
                $event->setAccount($account);
                $event->setOldPostTitle($oldPostTitle);
                $event->setForm($form);
                $event->setDate($now);
                $eventDispatcher->dispatch(Events::POST_UPDATE_BEFORE, $event);
                $isPublished = $event->isActionPublished();

                $em = $doctrine->getManager();
                $em->persist($post);
                $em->flush();

                /** @var TagRepository $tagRepo */
                $tagRepo = $doctrine->getRepository(Tag::class);
                $tags = [];
                foreach(Post::tagsStringToArray($post->getTags2()) as $itemKey => $tagNameItem) {
                    $tagX = $tagRepo->findOneByName($tagNameItem);
                    if(!$tagX) {
                        $tagX = new Tag();
                        $tagX->setName($tagNameItem);
                        $tags[] = $tagX;
                    }
                }
                if($tags) {
                    $post->setTags($tags);
                    $em->persist($post);
                    $em->flush();
                }
                $postSlug = $post->getSlug();

                $routeData = new EventData\RedirectToRoute();
                if($isPublished) {
                    $routeData->setRouteName('blog_post');
                    $routeData->setParameters(['slug' => $postSlug]);

                    //return $this->redirectToRoute('blog_post', ['slug' => $postSlug]);
                }
                else {
                    $routeData->setRouteName('blog_post_draft');
                    $routeData->setParameters(['slug' => $postSlug]);
                }
                $event = new PostUpdatedEvent($post);
                $event->setRouteData($routeData);
                $event->setIsActionPublished($isPublished);
                $eventDispatcher->dispatch(Events::POST_UPDATED, $event);

                return $this->redirectToRoute($routeData->getRouteName(), $routeData->getParameters());
            }
        }

        $dataCollection = new ArrayCollection();
        $params['dataCollection'] = $dataCollection;

        $response = new Response();
        $args['response'] = $response;

        $args = $params;
        $args['form'] = $form;

        $event = new PostCreateUpdateEvent($post, $args);
        $eventDispatcher->dispatch(Events::POST_UPDATE_RENDER_BEFORE, $event);

        $params['formView'] = $form->createView();
        $params['post'] = $post;

        $params = $params + (array)$dataCollection;

        return $this->render($templateName, $params, $response);
    }

    /**
     * @Route("/draft/{slug}", methods={"GET"}, name="blog_post_draft")
     * @Security("has_role('ROLE_BLOGGER')")
     *
     * NOTE: The $post controller argument is automatically injected by Symfony
     * after performing a database query looking for a Post with the 'slug'
     * value given in the route.
     * See https://symfony.com/doc/current/bundles/SensioFrameworkExtraBundle/annotations/converters.html
     */
    public function readDraft(EventDispatcherInterface $eventDispatcher,
                         LoggerInterface $logger,
                         TokenStorageInterface $tokenStorage,
                         string $slug
    ): Response
    {
        $doctrine = $this->getDoctrine();

        /** @var Account $account */
        $account = $tokenStorage->getToken()->getUser();

        /** @var PostRepository $postRepo */
        $postRepo = $doctrine->getRepository(Post::class);
        $post = $postRepo->findOneUnpublishedBySlugAndAuthor($slug, $account);
        if(!$post) {
            throw $this->createNotFoundException("Not found your post draft.");
        }
        // This time 'BEFORE' has no sense
        //$event = new GenericEvent($post);
        //$eventDispatcher->dispatch(Events::POST_READ_BEFORE, $event);

        // Symfony's 'dump()' function is an improved version of PHP's 'var_dump()' but
        // it's not available in the 'prod' environment to prevent leaking sensitive information.
        // It can be used both in PHP files and Twig templates, but it requires to
        // have enabled the DebugBundle. Uncomment the following line to see it in action:
        //
        // dump($post, $this->getUser(), new \DateTime());

        $params = [];
        $params['isDraft'] = true;
        $dataCollection = new ArrayCollection();
        $params['dataCollection'] = $dataCollection;

        $args = $params;

        $response = new Response();
        $args['response'] = $response;

        $event = new PostReadEvent($post, $args);
        $eventDispatcher->dispatch(Events::POST_READ_DRAFT_RENDER_BEFORE, $event);

        $params['post'] = $post;
        $params = $params + (array)$dataCollection;

        $response = $this->render(self::DESIGN_NAME . '/full/blog/post/read.html.twig',
            $params,
            $response
        );

        return $response;
    }

    /**
     * @Route("/{slug}", methods={"GET"}, name="blog_post")
     *
     * NOTE: The $post controller argument is automatically injected by Symfony
     * after performing a database query looking for a Post with the 'slug'
     * value given in the route.
     * See https://symfony.com/doc/current/bundles/SensioFrameworkExtraBundle/annotations/converters.html
     */
    public function read(EventDispatcherInterface $eventDispatcher,
                         LoggerInterface $logger,
                         TokenStorageInterface $tokenStorage,
                         string $slug
    ): Response
    {
        $doctrine = $this->getDoctrine();

        ///** @var Account $account */
        //$account = $tokenStorage->getToken()->getUser();

        /** @var PostRepository $postRepo */
        $postRepo = $doctrine->getRepository(Post::class);
        $post = $postRepo->findOnePublishedBySlug($slug);
        if(!$post) {
            throw $this->createNotFoundException("Not found published post.");
        }
        // This time 'BEFORE' has no sense
        //$event = new GenericEvent($post);
        //$eventDispatcher->dispatch(Events::POST_READ_BEFORE, $event);

        // Symfony's 'dump()' function is an improved version of PHP's 'var_dump()' but
        // it's not available in the 'prod' environment to prevent leaking sensitive information.
        // It can be used both in PHP files and Twig templates, but it requires to
        // have enabled the DebugBundle. Uncomment the following line to see it in action:
        //
        // dump($post, $this->getUser(), new \DateTime());
        $params = [];
        $params['isDraft'] = false;

        $dataCollection = new ArrayCollection();
        $params['dataCollection'] = $dataCollection;

        $args = $params;

        $response = new Response();
        $args['response'] = $response;

        $event = new PostReadEvent($post, $args);
        $eventDispatcher->dispatch(Events::POST_READ_RENDER_BEFORE, $event);

        $params['post'] = $post;
        $params = $params + (array)$dataCollection;

        $response = $this->render(self::DESIGN_NAME . '/full/blog/post/read.html.twig',
            $params,
            $response
        );

        return $response;
    }

    /**
     * @Route("/{postSlug}/comment/new", methods={"POST"}, name="comment_new")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     * @ParamConverter("post", options={"mapping": {"postSlug": "slug"}})
     *
     * NOTE: The ParamConverter mapping is required because the route parameter
     * (postSlug) doesn't match any of the Doctrine entity properties (slug).
     * See https://symfony.com/doc/current/bundles/SensioFrameworkExtraBundle/annotations/converters.html#doctrine-converter
     */
    public function commentNew(Request $request, Post $post, EventDispatcherInterface $eventDispatcher): Response
    {
        $comment = new Comment();
        $comment->setAuthor($this->getUser());
        $post->addComment($comment);

        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $event = new GenericEvent($comment, ['post' => $post]);
            $eventDispatcher->dispatch(Events::COMMENT_CREATE_BEFORE, $event);

            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();

            // When triggering an event, you can optionally pass some information.
            // For simple applications, use the GenericEvent object provided by Symfony
            // to pass some PHP variables. For more complex applications, define your
            // own event object classes.
            // See https://symfony.com/doc/current/components/event_dispatcher/generic_event.html
            $event = new GenericEvent($comment, ['post' => $post]);

            // When an event is dispatched, Symfony notifies it to all the listeners
            // and subscribers registered to it. Listeners can modify the information
            // passed in the event and they can even modify the execution flow, so
            // there's no guarantee that the rest of this controller will be executed.
            // See https://symfony.com/doc/current/components/event_dispatcher.html
            $eventDispatcher->dispatch(Events::COMMENT_CREATED, $event);

            return $this->redirectToRoute('blog_post', ['slug' => $post->getSlug()]);
        }

        return $this->render(self::DESIGN_NAME . '/full/blog/post/comment/form_error.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
        ]);
    }

    /**
     * This controller is called directly via the render() function in the
     * blog/post_show.html.twig template. That's why it's not needed to define
     * a route name for it.
     *
     * The "id" of the Post is passed in and then turned into a Post object
     * automatically by the ParamConverter.
     */
    public function commentForm(Post $post): Response
    {
        $form = $this->createForm(CommentType::class);

        return $this->render(self::DESIGN_NAME . '/parts/blog/post/comment/form.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/search", methods={"GET"}, name="blog_post_search")
     */
    public function search(Request $request, PostRepository $postsRepo): Response
    {
        if (!$request->isXmlHttpRequest()) {
            return $this->render('blog/search.html.twig');
        }

        $query = $request->query->get('q', '');
        $limit = $request->query->get('l', 10);
        $foundPosts = $postsRepo->findBySearchQuery($query, $limit);

        $results = [];
        foreach ($foundPosts as $post) {
            $results[] = [
                'title' => htmlspecialchars($post->getTitle(), ENT_COMPAT | ENT_HTML5),
                'date' => $post->getPublishedAt()->format('M d, Y'),
                'author' => htmlspecialchars($post->getAuthor()->getFullName(), ENT_COMPAT | ENT_HTML5),
                'summary' => htmlspecialchars($post->getSummary(), ENT_COMPAT | ENT_HTML5),
                'url' => $this->generateUrl('blog_post', ['slug' => $post->getSlug()]),
            ];
        }

        return $this->json($results);
    }


    public function esiRead(EventDispatcherInterface $eventDispatcher,
                            LoggerInterface $logger,
                            TokenStorageInterface $tokenStorage,
                            int $postId,
                            string $template = self::DESIGN_NAME . '/full/blog/post/parts/empty-template.html.twig'
    ): Response
    {
        $doctrine = $this->getDoctrine();

        ///** @var Account $account */
        //$account = $tokenStorage->getToken()->getUser();

        /** @var PostRepository $postRepo */
        $postRepo = $doctrine->getRepository(Post::class);
        $post = $postRepo->findOnePublished($postId);

        $isDraft = false;
        if(!$post) {
            $post = $postRepo->findOneUnpublished($postId);
            if($post) {
                $isDraft = true;
            }
        }


        $response = new Response();
        $response->setPrivate();
        if(!$post) {
            //throw $this->createNotFoundException("Not found published post.");

            return $response;
        }

        $canUpdate = false;
        $token = $tokenStorage->getToken();
        if($token) {
            /** @var Account|string $account */
            $account = $token->getUser();
            if(is_object($account) and $account->hasRole('ROLE_BLOGGER')) {
                $author = $post->getAuthor();
                if($author) {
                    if($author->getId() == $account->getId()) {
                        $canUpdate = true;
                    }
                }
                if (!$canUpdate) {
                    $modifier = $post->getModifier();
                    if($modifier) {
                        if($modifier->getId() == $account->getId()) {
                            $canUpdate = true;
                        }
                    }
                }
            }
        }

        // Has no draft in array because post will be the event subject
        $args = [
            'canUpdate' => $canUpdate,
            'isDraft' => $isDraft,
            'dataCollection' => new ArrayCollection()
        ];

        $args['response'] = $response;
        $event = new PostReadEvent($post, $args);
        if(!$isDraft) {
            $eventDispatcher->dispatch(Events::POST_READ_ESI, $event);
        }
        else {
            $eventDispatcher->dispatch(Events::POST_READ_DRAFT_ESI, $event);
        }

        $params = $args;
        $params['post'] = $post;
        $response = $this->render($template,
            $params,
            $response
        );

        return $response;
    }
}
