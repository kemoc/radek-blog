<?php


namespace RadekZ\Blog\BackendBundle\EventDispatcher;


use RadekZ\Blog\BackendBundle\EventData\RedirectToRoute;
//use RadekZ\Blog\BackendBundle\Entity\Post;

/**
 * Class PostCreateUpdateEvent
 *
 * @inheritdoc
 */
class PostCreatedEvent extends PostReadEvent
{
    /**
     * @var RedirectToRoute
     */
    protected $routeData;
    /**
     * @var bool
     */
    protected $isActionPublished = false;

    /**
     * @return RedirectToRoute
     */
    public function getRouteData(): RedirectToRoute
    {
        return $this->routeData;
    }

    /**
     * @param RedirectToRoute $routeData
     */
    public function setRouteData(RedirectToRoute $routeData): void
    {
        $this->routeData = $routeData;
    }

    /**
     * @return bool
     */
    public function isActionPublished(): bool
    {
        return $this->isActionPublished;
    }

    /**
     * @param bool $isActionPublished
     */
    public function setIsActionPublished(bool $isActionPublished): void
    {
        $this->isActionPublished = $isActionPublished;
    }
}
