<?php
declare(strict_types=1);


namespace RadekZ\Blog\BackendBundle\EventDispatcher;

use Symfony\Component\EventDispatcher\GenericEvent;
use RadekZ\Blog\BackendBundle\Entity\Post;

/**
 * Class PostReadEvent
 * @package RadekZ\Blog\BackendBundle\EventDispatcher
 *
 * @method Post getSubject()
 */
class PostReadEvent extends GenericEvent
{

}
