<?php
declare(strict_types=1);


namespace RadekZ\Blog\BackendBundle\EventDispatcher;


use DateTime;


class PostUpdateBeforeEvent extends PostCreateBeforeEvent
{
    /**
     * @var DateTime
     */
    protected $date;
    /**
     * @var string
     */
    protected $oldPostTitle;

    /**
     * @return DateTime
     */
    public function getDate(): DateTime
    {
        return $this->date;
    }

    /**
     * @param DateTime $date
     */
    public function setDate(DateTime $date): void
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getOldPostTitle(): string
    {
        return $this->oldPostTitle;
    }

    /**
     * @param string $oldPostTitle
     */
    public function setOldPostTitle(string $oldPostTitle): void
    {
        $this->oldPostTitle = $oldPostTitle;
    }

}

