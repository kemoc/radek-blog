<?php
declare(strict_types=1);


namespace RadekZ\Blog\BackendBundle\EventDispatcher;

use Symfony\Component\Form\FormInterface;
use RadekZ\Blog\BackendBundle\Entity\Account;

/**
 * Class PostCreateBeforeEvent
 * @package RadekZ\Blog\BackendBundle\EventDispatcher
 * @inheritdoc
 */
class PostCreateBeforeEvent extends PostCreateUpdateEvent
{
    /**
     * @var FormInterface
     */
    protected $form;

    /**
     * @var Account
     */
    protected $account;
    /**
     * @var bool
     */
    protected $isActionPublished = false;

    public function setForm(FormInterface $form): void
    {
    	$this->form = $form;
    }
    public function getForm(): FormInterface
    {

    	return $this->form;
    }

    /**
     * @return Account
     */
    public function getAccount(): Account
    {
        return $this->account;
    }

    /**
     * @param Account $account
     */
    public function setAccount(Account $account): void
    {
        $this->account = $account;
    }

    /**
     * @return bool
     */
    public function isActionPublished(): bool
    {
        return $this->isActionPublished;
    }

    /**
     * @param bool $isActionPublished
     */
    public function setIsActionPublished(bool $isActionPublished): void
    {
        $this->isActionPublished = $isActionPublished;
    }

}

