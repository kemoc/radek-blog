<?php
declare(strict_types=1);


namespace RadekZ\Blog\BackendBundle;

use RadekZ\Blog\BackendBundle\EventDispatcher\PostCreateUpdateEvent;
use RadekZ\Blog\BackendBundle\EventDispatcher\PostReadEvent;

class Events
{
    /**
     * For the event naming conventions, see:
     * https://symfony.com/doc/current/components/event_dispatcher.html#naming-conventions.
     *
     * @Event("Symfony\Component\EventDispatcher\GenericEvent")
     *
     * @var string
     */
    public const COMMENT_CREATE_BEFORE = 'comment.create.before';
    /**
     *
     * @Event("Symfony\Component\EventDispatcher\GenericEvent")
     *
     * @var string
     */
    public const COMMENT_CREATED = 'comment.created';

    /**
     *
     * @Event("RadekZ\Blog\BackendBundle\EventDispatcher\PostCreateBeforeEvent")
     *
     * @var string
     */
    public const POST_CREATE_BEFORE = 'post.create.before';
    /**
     *
     * @Event("RadekZ\Blog\BackendBundle\EventDispatcher\PostCreatedEvent")
     *
     * @var string
     */
    public const POST_CREATED = 'post.created';

    /**
     *
     * @Event("RadekZ\Blog\BackendBundle\EventDispatcher\PostUpdateBeforeEvent")
     *
     * @var string
     */

    public const POST_UPDATE_BEFORE = 'post.update.before';
    /**
     *
     * @Event("RadekZ\Blog\BackendBundle\EventDispatcher\PostUpdatedEvent")
     *
     * @var string
     */
    public const POST_UPDATED = 'post.updated';

    /**
     *
     * @Event("RadekZ\Blog\BackendBundle\EventDispatcher\PostReadEvent")
     *
     * @var string
     */
    public const POST_READ_BEFORE = 'post.read.before';
    /**
     *
     * @Event("RadekZ\Blog\BackendBundle\EventDispatcher\PostReadEvent")
     *
     * @var string
     */
    public const POST_READ = 'post.read';
    /**
     *
     * @Event("RadekZ\Blog\BackendBundle\EventDispatcher\PostReadEvent")
     *
     * @var string
     */
    public const POST_READ_RENDER_BEFORE = 'post.read.render.before';

    /**
     *
     * @Event("RadekZ\Blog\BackendBundle\EventDispatcher\PostReadEvent")
     *
     * @var string
     */
    public const POST_READ_DRAFT_RENDER_BEFORE = 'post.read.draft.render.before';
    /**
     *
     * @Event("RadekZ\Blog\BackendBundle\EventDispatcher\PostReadEvent")
     *
     * @var string
     */
    public const POST_READ_ESI = 'post.read.esi';
    /**
     *
     * @Event("RadekZ\Blog\BackendBundle\EventDispatcher\PostReadEvent")
     *
     * @var string
     */
    public const POST_READ_DRAFT_ESI = 'post.read.draft.esi';

    /**
     *
     * @Event("Symfony\Component\EventDispatcher\GenericEvent")
     *
     * @var string
     */
    public const POST_DELETE_BEFORE = 'post.delete.before';
    /**
     *
     * @Event("RadekZ\Blog\BackendBundle\EventDispatcher\PostCreateUpdateEvent")
     *
     * @var string
     */
    public const POST_DELETED = 'post.deleted';
    /**
     *
     * @Event("Symfony\Component\EventDispatcher\GenericEvent")
     *
     * @var string
     */
    public const POSTS_SEARCHED_BY_TAG_BEFORE = 'posts.searched.by.tag.before';

    /**
     *
     * @Event("RadekZ\Blog\BackendBundle\EventDispatcher\PostCreateUpdateEvent")
     *
     * @var string
     */
    public const POST_CREATE_RENDER_BEFORE = 'post.create.render.before';
    /**
     *
     * @Event("RadekZ\Blog\BackendBundle\EventDispatcher\PostCreateUpdateEvent")
     *
     * @var string
     */
    public const POST_UPDATE_RENDER_BEFORE = 'post.update.render.before';
}
