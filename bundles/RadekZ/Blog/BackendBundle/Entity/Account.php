<?php
declare(strict_types=1);

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace RadekZ\Blog\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\User as AbstractAccount;
use FOS\UserBundle\Model\GroupInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity(repositoryClass="RadekZ\Blog\BackendBundle\Repository\AccountRepository")
 * @ORM\Table(name="account")
 * @UniqueEntity(fields="email", message="Email already taken")
 * @UniqueEntity(fields="username", message="Login already taken")
 *
 * Defines the properties of the Account entity to represent the application users.
 * See https://symfony.com/doc/current/book/doctrine.html#creating-an-entity-class
 *
 * Tip: if you have an existing database, you can generate these entity class automatically.
 * See https://symfony.com/doc/current/cookbook/doctrine/reverse_engineering.html
 *
 * @author Ryan Weaver <weaverryan@gmail.com>
 * @author Javier Eguiluz <javier.eguiluz@gmail.com>
 */
class Account extends AbstractAccount implements \Serializable
{
    public const ENABLED = 1;
    public const DISABLED = 0;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=512, nullable=true)
     */
    protected $fullName = null;
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Assert\NotNull()
     * @Assert\Length(min="6", max="255")
     */
    protected $nickname;
    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotNull()
     * @Assert\Length(min="2", max="255")
     */
    protected $firstname;
    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotNull()
     * @Assert\Length(min="2", max="255")
     */
    protected $lastname;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    protected $invalidLoginCounter;


    public function __construct()
    {
        parent::__construct();
        $this->setInvalidLoginCounter(0);
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    public function setNickname(string $nickname): self
    {
        $this->nickname = $nickname;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function setFullName(string $fullName): self
    {
        $this->fullName = $fullName;

        return $this;
    }

    public function getFullName(): string
    {
        return $this->getFirstname() . " " . $this->getLastname();
    }

    /**
     * @return int
     */
    public function getInvalidLoginCounter(): int
    {
        return $this->invalidLoginCounter;
    }

    /**
     * @param int $invalidLoginCounter
     * @return Account
     */
    public function setInvalidLoginCounter(int $invalidLoginCounter): self
    {
        $this->invalidLoginCounter = $invalidLoginCounter;

        return $this;
    }

}
