<?php
declare(strict_types=1);

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace RadekZ\Blog\BackendBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="RadekZ\Blog\BackendBundle\Repository\TagRepository")
 * @ORM\Table(name="tag")
 *
 * Defines the properties of the Tag entity to represent the post tags.
 *
 * See https://symfony.com/doc/current/book/doctrine.html#creating-an-entity-class
 *
 * @author Yonel Ceruto <yonelceruto@gmail.com>
 */
class Tag implements \JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", unique=true)
     */
    private $name;
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    protected $searchedByTagNumber = 0;

    /**
     * @ORM\ManyToMany(targetEntity="\RadekZ\Blog\BackendBundle\Entity\Post", mappedBy="tags")
     *
     * @var Post[]|ArrayCollection
     */
    private $posts;

    public function getId(): int
    {
        return $this->id;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize(): string
    {
        // This entity implements JsonSerializable (http://php.net/manual/en/class.jsonserializable.php)
        // so this method is used to customize its JSON representation when json_encode()
        // is called, for example in tags|json_encode (app/Resources/views/form/fields.html.twig)

        return $this->name;
    }

    public function __toString(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getSearchedByTagNumber(): int
    {
        return $this->searchedByTagNumber;
    }

    /**
     * @param int $searchByTagNumber
     */
    public function setSearchedByTagNumber(int $searchedByTagNumber): void
    {
        $this->searchedByTagNumber = $searchedByTagNumber;
    }


    /**
     * @return ArrayCollection|Post[]
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @param ArrayCollection|Post[] $posts
     */
    public function setPosts($posts): void
    {
        $this->posts = $posts;
    }


}
