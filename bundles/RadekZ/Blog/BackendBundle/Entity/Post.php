<?php
declare(strict_types=1);

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace RadekZ\Blog\BackendBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="RadekZ\Blog\BackendBundle\Repository\PostRepository")
 * @ORM\Table(name="post")
 *
 * Defines the properties of the Post entity to represent the blog posts.
 *
 * See https://symfony.com/doc/current/book/doctrine.html#creating-an-entity-class
 *
 * Tip: if you have an existing database, you can generate these entity class automatically.
 * See https://symfony.com/doc/current/cookbook/doctrine/reverse_engineering.html
 *
 * @author Ryan Weaver <weaverryan@gmail.com>
 * @author Javier Eguiluz <javier.eguiluz@gmail.com>
 * @author Yonel Ceruto <yonelceruto@gmail.com>
 */
class Post
{
    /**
     * Use constants to define configuration options that rarely change instead
     * of specifying them under parameters section in config/services.yaml file.
     *
     * See https://symfony.com/doc/current/best_practices/configuration.html#constants-vs-configuration-options
     */
    public const NUM_ITEMS = 10;

    public const STATUS_NEW = 0;
    public const STATUS_HIDDEN = 2;
    public const STATUS_EDITING = 4;
    public const STATUS_SAVED_DRAFT = 8;
    public const STATUS_PUBLISHED = 1024;
    public const STATUS_DELETED = 512;
    public const STATUSES = [
        self::STATUS_NEW,
        self::STATUS_HIDDEN,
        self::STATUS_EDITING,
        self::STATUS_SAVED_DRAFT,
        self::STATUS_DELETED,
        self::STATUS_PUBLISHED
    ];

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=700)
     * @Assert\NotBlank
     * @Assert\Length(min="2", max="700")
     */
    private $title;

    /**
     * @var string
     *
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(type="string", length=700, unique=true)
     * @Assert\NotBlank
     * @Assert\Length(min="2", max="700")
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="post.blank_content")
     * @Assert\Length(min=10, minMessage="post.too_short_content")
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=4000, nullable=true)
     * @Assert\Length(max=4000)
     */
    private $summary;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     * @Assert\DateTime
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     * @Assert\DateTime
     */
    private $publishedAt;

    /**
     * @var null|Account
     *
     * @ORM\ManyToOne(targetEntity="Account")
     * @ORM\JoinColumn(nullable=true)
     */
    private $author;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     * @Assert\DateTime
     */
    private $modified;

    /**
     * @var null|Account
     *
     * @ORM\ManyToOne(targetEntity="Account")
     * @ORM\JoinColumn(nullable=true)
     */
    private $modifier;
    /**
     * @ORM\Column(type="string", nullable=false)
     * @Assert\Length(max=255)
     *
     * @var string
     */
    private $modifierNickName;

    /**
     * @var Comment[]|ArrayCollection
     *
     * @ORM\OneToMany(
     *      targetEntity="Comment",
     *      mappedBy="post",
     *      orphanRemoval=true,
     *      cascade={"persist"}
     * )
     * @ORM\OrderBy({"publishedAt": "DESC"})
     */
    private $comments;

    /**
     * @var Tag[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="\RadekZ\Blog\BackendBundle\Entity\Tag",
     *     cascade={"persist"},
     *     inversedBy="posts"
     * )
     * @ORM\JoinColumn(nullable=true)
     * @ORM\OrderBy({"name": "ASC"})
     * @Assert\Count(min="1", max="9", maxMessage="post.too_many_tags")
     */
    private $tags;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1000)
     * @Assert\NotBlank(message="post.blank_tags")
     * @Assert\Length(min="2", max="1000")
     */
    private $tags2;

    /**
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $viewReadNumber = 0;


    /**
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $status = self::STATUS_NEW;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Assert\Length(max=255)
     *
     * @var string
     */
    private $authorNickname;


    public function __construct()
    {
        $created = new \DateTime();
        $this->created = $created;
        $this->publishedAt = clone $created;
        $this->modified = clone $created;
        $this->comments = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->status = self::STATUS_NEW;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): void
    {
        $this->slug = $slug;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): void
    {
        $this->content = $content;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created): void
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getModified(): \DateTime
    {
        return $this->modified;
    }

    /**
     * @param \DateTime $modified
     */
    public function setModified(\DateTime $modified): void
    {
        $this->modified = $modified;
    }

    public function getPublishedAt(): \DateTime
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(?\DateTime $publishedAt): void
    {
        $this->publishedAt = $publishedAt;
    }

    public function getAuthor(): ?Account
    {
        return $this->author;
    }

    public function setAuthor(?Account $author): void
    {
        $this->author = $author;
    }

    public function getModifier(): ?Account
    {
        return $this->modifier;
    }

    public function setModifier(Account $modifier): void
    {
        $this->modifier = $modifier;
    }

    public function getModifierNickName(): string
    {
        if($this->getModifier()) {

            return $this->getModifier()->getNickname();
        }

        return $this->modifierNickName;
    }

    public function setModifierNickName(string $modifierNickName): void
    {
        $this->modifierNickName = $modifierNickName;
    }

    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(?Comment $comment): void
    {
        $comment->setPost($this);
        if (!$this->comments->contains($comment)) {
            $this->comments->add($comment);
        }
    }

    public function removeComment(Comment $comment): void
    {
        $comment->setPost(null);
        $this->comments->removeElement($comment);
    }

    public function getSummary(): string
    {
        return (string)$this->summary;
    }

    public function setSummary(string $summary = ""): void
    {
        $this->summary = $summary;
    }

    public function addTag(?Tag ...$tags): void
    {
        foreach ($tags as $tag) {
            if (!$this->tags->contains($tag)) {
                $this->tags->add($tag);
            }
        }
    }

    public function removeTag(Tag $tag): void
    {
        $this->tags->removeElement($tag);
    }

    /**
     * @return ArrayCollection|Tag[]
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param ArrayCollection|Tag[] $array
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
    }

    public function getTags2($clean = false): string
    {
        if($clean) {

            return trim($this->tags2, ' ,');
        }
        return $this->tags2;
    }

    /**
     * @param string $tags2
     */
    public function setTags2(string $tags2): void
    {
        $this->tags2 = "," . trim($tags2, ' ,') . ",";
    }


    /**
     * @return int
     */
    public function getViewReadNumber(): int
    {
        return $this->viewReadNumber;
    }

    /**
     * @param int $viewReadNumber
     */
    public function setViewReadNumber(int $viewReadNumber): void
    {
        $this->viewReadNumber = $viewReadNumber;
    }

    /**
     * @return string[]
     */
    public function getTagsArray(): array
    {

    	return explode(',', trim($this->tags2, ', '));
    }

    /**
     * @param string[] $tags
     */
    public function setTagsFromArray(array $tags = array())
    {
    	$this->tags2 = $this->tagsStringToArray($tags);
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    public function getAuthorNickname(): string
    {
        if(!$this->getAuthor()) {

            return $this->authorNickname;
        }


        return $this->getAuthor()->getNickname();
    }

    public function setAuthorNickname(string $authorNickname): void
    {
        $this->authorNickname = $authorNickname;
    }

    // -------------------------------------------------------------

    public static function tagsStringToArray(string $tags)
    {

    	return explode(',', trim($tags, ', '));
    }

    public static function generateSlugSuffix(int $suffix): ?int
    {
    	if($suffix < 2) {

    	    return null;
        }


        return (int)$suffix;
    }

    // --------------------------------------------------------------------
}
