<?php
declare(strict_types=1);


namespace RadekZ\Blog\BackendBundle\Form\Data\Post;


use Doctrine\Common\Collections\ArrayCollection;
use RadekZ\Blog\BackendBundle\Entity\Post as PostEntity;
use RadekZ\Blog\BackendBundle\Entity\Tag;
use Symfony\Component\Translation\TranslatorInterface;

trait CreateUpdateTrait
{
    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var PostEntity
     */
    protected $postEntity;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min="2", max="255")
     * @var null|string
     */
    protected $title;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min="5")
     * @var null|string
     */
    protected $content;

    /**
     * @var null|string
     */
    protected $summary;

    /**
     * @var null|string
     */
    protected $tags2;


    /**
     * @param PostEntity $postEntity
     */
    public function setPostEntity(PostEntity $postEntity): void
    {
        $this->postEntity = $postEntity;
    }

    /**
     * @return PostEntity
     */
    public function getPostEntity(): PostEntity
    {

        return $this->postEntity;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getTitle(): ?string
    {

        return $this->title;
    }

    /**
     * @return string
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getSummary(): ?string
    {
        return $this->summary;
    }

    /**
     * @param null|string $summary
     */
    public function setSummary(?string $summary): void
    {
        $this->summary = $summary;
    }

    /**
     * @return null|string
     */
    public function getTags2(): ?string
    {
        return $this->tags2;
    }

    /**
     * @param null|string $tags2
     */
    public function setTags2(?string $tags2): void
    {
        $this->tags2 = $tags2;
    }


    public function copyTo(PostEntity $post)
    {
        $post->setTitle($this->getTitle());
        $post->setContent($this->getContent());
        $post->setSummary((string)$this->getSummary());
        $post->setTags2($this->getTags2());
    }
}

