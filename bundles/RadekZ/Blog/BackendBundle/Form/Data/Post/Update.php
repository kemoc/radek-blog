<?php
declare(strict_types=1);


namespace RadekZ\Blog\BackendBundle\Form\Data\Post;

use Doctrine\Common\Collections\ArrayCollection;
use RadekZ\Blog\BackendBundle\Entity\Tag;
use Symfony\Component\Validator\Constraints as Assert;
use RadekZ\Blog\BackendBundle\Entity\Post as PostEntity;
use Symfony\Component\Translation\TranslatorInterface;

class Update implements CreateInterface
{
    use CreateUpdateTrait;

    /**
     * @Assert\Range(min="1")
     * @Assert\NotBlank()
     *
     * @var int
     */
    protected $id = 0;

    public function __construct(PostEntity $postEntity, TranslatorInterface $translator)
    {
        $this->postEntity = $postEntity;

        $this->setId($postEntity->getId());

        $this->setTitle($postEntity->getTitle());

        $this->setContent($postEntity->getContent());

        $this->setSummary($postEntity->getSummary());

        $this->setTags2($postEntity->getTags2(true));
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }



}

