<?php
declare(strict_types=1);


namespace RadekZ\Blog\BackendBundle\Form\Data\Post;

use Doctrine\Common\Collections\ArrayCollection;
use RadekZ\Blog\BackendBundle\Entity\Account;
use RadekZ\Blog\BackendBundle\Entity\Comment;
use RadekZ\Blog\BackendBundle\Entity\Tag;
use Symfony\Component\Validator\Constraints as Assert;
use RadekZ\Blog\BackendBundle\Entity\Post as PostEntity;
use Symfony\Component\Translation\TranslatorInterface;
use DateTime;

interface CreateInterface
{

    public function __construct(PostEntity $postEntity, TranslatorInterface $translator);

    /**
     * @param PostEntity $postEntity
     */
    public function setPostEntity(PostEntity $postEntity): void;

    /**
     * @return PostEntity
     */
    public function getPostEntity(): PostEntity;

    /**
     * @return string
     */
    public function getTitle(): ?string;

    /**
     * @param string $username
     */
    public function setTitle(string $title): void;

    public function getContent(): ?string;

    public function setContent(string $content): void;

    public function getSummary(): ?string;

    public function setSummary(string $summary): void;

    public function getTags2(): ?string;

    public function setTags2(?string $tags): void;
}

