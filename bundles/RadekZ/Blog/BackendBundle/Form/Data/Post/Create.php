<?php
declare(strict_types=1);


namespace RadekZ\Blog\BackendBundle\Form\Data\Post;

use Doctrine\Common\Collections\ArrayCollection;
use RadekZ\Blog\BackendBundle\Entity\Tag;
use Symfony\Component\Validator\Constraints as Assert;
use RadekZ\Blog\BackendBundle\Entity\Post as PostEntity;
use Symfony\Component\Translation\TranslatorInterface;

class Create implements CreateInterface
{
    use CreateUpdateTrait;

    public function __construct(PostEntity $postEntity, TranslatorInterface $translator)
    {
        $this->postEntity = $postEntity;

    }


}

