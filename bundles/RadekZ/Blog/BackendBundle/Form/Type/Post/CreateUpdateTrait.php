<?php
declare(strict_types=1);


namespace RadekZ\Blog\BackendBundle\Form\Type\Post;

use RadekZ\Blog\BackendBundle\Entity\Tag;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form;
use Symfony\Component\Form\Extension\Core\Type;

Trait CreateUpdateTrait
{
    public function buildForm(Form\FormBuilderInterface $builder, array $options): void
    {
        $builder->add('title', Type\TextType::class,
            [
                'label' => 'Tytuł',
                'required' => true
            ]
        )->add('content', Type\TextareaType::class,
            [
                'label' => "Treść",
                'required' => true
            ]
        )->add('summary', Type\TextareaType::class,
            [
                'label' => "Podsumowanie"
            ]
        )->add('tags2', Type\TextType::class,
            [
                'label' => "Tags",
                'required' => true,
                'mapped' => true,
            ]

        )->add(CreateType::SUBMIT_SAVE_DRAFT, Type\SubmitType::class,
            [
                'label' => "Zapisz"
            ]
        )->add(CreateType::SUBMIT_SAVE_AND_PUBLISH, Type\SubmitType::class,
            [
                'label' => "Zapisz i Publikuj"
            ]
        )
        ;
    }
}

