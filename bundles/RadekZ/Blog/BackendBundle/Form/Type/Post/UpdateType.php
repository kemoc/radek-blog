<?php
declare(strict_types=1);


namespace RadekZ\Blog\BackendBundle\Form\Type\Post;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form;
use Symfony\Component\Form\Extension\Core\Type;

class UpdateType extends Form\AbstractType
{
    use CreateUpdateTrait;
}

