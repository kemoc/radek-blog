<?php
declare(strict_types=1);


namespace RadekZ\Blog\BackendBundle\Form\Type\Post;

use RadekZ\Blog\BackendBundle\Entity\Tag;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form;
use Symfony\Component\Form\Extension\Core\Type;

class CreateType extends Form\AbstractType
{
    public const SUBMIT_SAVE_DRAFT = "SaveDraft";
    public const SUBMIT_SAVE_AND_PUBLISH = "SaveAndPublish";

    use CreateUpdateTrait;
}

