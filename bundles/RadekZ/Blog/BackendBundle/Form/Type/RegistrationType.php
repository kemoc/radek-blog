<?php
declare(strict_types=1);


namespace RadekZ\Blog\BackendBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type;

class RegistrationType extends RegistrationFormType
{
    /**
     * @var string
     */
    protected $class;

    /**
     * @param string $class The User class name
     */
    public function __construct($class)
    {
        $this->class = $class;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('username', null,
            [
                'label' => 'form.login', 'translation_domain' => 'FOSUserBundle']
        );

        $builder->add('nickname', Type\TextType::class,
            [
                'label' => 'form.nickname',
                'translation_domain' => 'FOSUserBundle',
                'required' => true,
            ]
        );
        $builder->add('firstname', Type\TextType::class,
            [
                'label' => 'form.firstname',
                'translation_domain' => 'FOSUserBundle',
                'required' => true,
            ]
        );
        $builder->add('lastname', Type\TextType::class,
            [
                'label' => 'form.lastname',
                'translation_domain' => 'FOSUserBundle',
                'required' => true,
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => $this->class,
            'csrf_token_id' => 'registration',
        ));
    }

    public function getParent()
    {
        return RegistrationFormType::class;
    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }

    // For Symfony 2.x
    public function getName()
    {
        return $this->getBlockPrefix();
    }
}

