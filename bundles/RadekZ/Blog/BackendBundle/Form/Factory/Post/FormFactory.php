<?php
declare(strict_types=1);


namespace RadekZ\Blog\BackendBundle\Form\Factory\Post;

use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Util\StringUtil;
use Symfony\Component\OptionsResolver\Exception\InvalidOptionsException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

use RadekZ\Blog\BackendBundle\Form\Data\Post\Create;
use RadekZ\Blog\BackendBundle\Form\Type\Post\CreateType;
use RadekZ\Blog\BackendBundle\Form\Data\Post\Update;
use RadekZ\Blog\BackendBundle\Form\Type\Post\UpdateType;

class FormFactory
{

    /** @var FormFactoryInterface */
    protected $formFactory;

    /** @var UrlGeneratorInterface */
    protected $urlGenerator;

    public function __construct(FormFactoryInterface $formFactory, UrlGeneratorInterface $urlGenerator)
    {
        $this->formFactory = $formFactory;
        $this->urlGenerator = $urlGenerator;
    }

    public function getCreate(Create $data, ?string $name = null): FormInterface
    {
        $name = $name ?: StringUtil::fqcnToBlockPrefix(CreateType::class);

        return $this->formFactory->createNamed($name, CreateType::class, $data);
    }

    public function getUpdate(Update $data, ?string $name = null): FormInterface
    {
        $name = $name ?: StringUtil::fqcnToBlockPrefix(UpdateType::class);

        return $this->formFactory->createNamed($name, UpdateType::class, $data);
    }
}

