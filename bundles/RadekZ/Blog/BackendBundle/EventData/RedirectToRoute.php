<?php
declare(strict_types=1);

namespace RadekZ\Blog\BackendBundle\EventData;


class RedirectToRoute
{
    /**
     * @var string
     */
    protected $routeName;
    /**
     * @var array
     */
    protected $parameters = [];

    /**
     * @return string
     */
    public function getRouteName(): string
    {
        return $this->routeName;
    }

    /**
     * @param string $routeName
     */
    public function setRouteName(string $routeName): void
    {
        $this->routeName = $routeName;
    }

    /**
     * @return array
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * @param array $parameters
     */
    public function setParameters(array $parameters): void
    {
        $this->parameters = $parameters;
    }


}
