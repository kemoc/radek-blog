<?php
declare(strict_types=1);

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace RadekZ\Blog\BackendBundle\EventSubscriber;

use RadekZ\Blog\BackendBundle\Repository\AccountRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Doctrine\Common\Persistence\ManagerRegistry;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FilterUserResponseEvent;

/**
 * Notifies post's author about new comments.
 *
 * @author Oleg Voronkovich <oleg-voronkovich@yandex.ru>
 */
class RegistrationSubscriber implements EventSubscriberInterface
{
    protected $mailer;
    protected $translator;
    protected $urlGenerator;
    protected $sender;
    /**
     * @var ManagerRegistry
     */
    protected $managerRegistry;
    /**
     * @var string
     */
    protected $defaultAccountRole;


    public function __construct(\Swift_Mailer $mailer, UrlGeneratorInterface $urlGenerator,
                                TranslatorInterface $translator,
                                ManagerRegistry $managerRegistry,
                                string $defaultAccountRole
    )
    {
        $this->mailer = $mailer;
        $this->urlGenerator = $urlGenerator;
        $this->translator = $translator;
        //$this->sender = $sender;
        $this->managerRegistry = $managerRegistry;
        $this->defaultAccountRole = $defaultAccountRole;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            FOSUserEvents::REGISTRATION_COMPLETED => 'onRegistrationCompleted',
        ];
    }

    public function onRegistrationCompleted(FilterUserResponseEvent $event): void
    {
        $account = $event->getUser();
        if($account->hasRole($this->defaultAccountRole)) {

            return ;
        }
        $account->addRole($this->defaultAccountRole);

        $em = $this->managerRegistry->getManager();
        $em->persist($account);

        $em->flush();
    }

}
