<?php
declare(strict_types=1);


namespace RadekZ\Blog\BackendBundle\EventSubscriber;

use Psr\Log\LoggerInterface;
use RadekZ\Blog\BackendBundle\Entity\Account;
use RadekZ\Blog\BackendBundle\Repository\AccountRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\AuthenticationEvents;
use Symfony\Component\Security\Core\Event\AuthenticationEvent;
use Symfony\Component\Security\Core\Event\AuthenticationFailureEvent;
use Symfony\Component\Translation\TranslatorInterface;
use Doctrine\Common\Persistence\ManagerRegistry;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FilterUserResponseEvent;

class AccountSubscriber implements EventSubscriberInterface
{
    protected $translator;
    protected $urlGenerator;
    /**
     * @var ManagerRegistry
     */
    protected $managerRegistry;
    protected $logger;
    protected $allowedFailedLoginCount;

    public function __construct(UrlGeneratorInterface $urlGenerator,
                                TranslatorInterface $translator,
                                ManagerRegistry $managerRegistry,
                                LoggerInterface $logger,
                                int $allowedFailedLoginCount
    )
    {
        $this->urlGenerator = $urlGenerator;
        $this->translator = $translator;
        //$this->sender = $sender;
        $this->managerRegistry = $managerRegistry;
        $this->logger = $logger;
        $this->allowedFailedLoginCount = $allowedFailedLoginCount;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            AuthenticationEvents::AUTHENTICATION_FAILURE => 'onAuthenticationFailure',
            AuthenticationEvents::AUTHENTICATION_SUCCESS => 'onAuthenticationSuccess',
        ];
    }
    
    public function onAuthenticationFailure(AuthenticationFailureEvent $event): void
    {
    	$token = $event->getAuthenticationToken();
    	/** @var Account $account */
        $account = $token->getUser();

        $em = $this->managerRegistry->getManager();

        if(!is_object($account)) {
            $username = $token->getUsername();
            $cred = $token->getCredentials();

            /** @var AccountRepository $accountRepo */
            $accountRepo = $em->getRepository(Account::class);
            $account = $accountRepo->findByUsername($username);
            if(!$account) {

                return ;
            }
        }

        $account->setInvalidLoginCounter($account->getInvalidLoginCounter() + 1);
        if($account->getInvalidLoginCounter() > 5) {
            $account->setEnabled(Account::DISABLED);
        }
        $em->persist($account);
        $em->flush();
    }

    public function onAuthenticationSuccess(AuthenticationEvent $event): void
    {
        $token = $event->getAuthenticationToken();
        /** @var Account $account */
        $account = $token->getUser();
        if(!is_object($account)) {

            return ;
        }
        if(!$account->isEnabled()) {
            return ;
        }
        $account->setInvalidLoginCounter(0);
        $em = $this->managerRegistry->getManager();
        $em->persist($account);
        $em->flush();
    }

}

