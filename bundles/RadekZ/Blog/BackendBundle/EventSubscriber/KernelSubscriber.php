<?php
declare(strict_types=1);


namespace RadekZ\Blog\BackendBundle\EventSubscriber;

use RadekZ\Blog\BackendBundle\Service\URLService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class KernelSubscriber implements EventSubscriberInterface
{
    /**
     * @var URLService
     */
    protected $URLService;


    public function __construct(URLService $URLService)
    {
        $this->URLService = $URLService;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => ['onKernelRequestSetPreviousPage', -50],
        ];
    }

    public function onKernelRequestSetPreviousPage(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        $this->URLService->addPageURLByRequest($request);
    }
}

