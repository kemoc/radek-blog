<?php
declare(strict_types=1);


namespace RadekZ\Blog\BackendBundle\EventSubscriber;

use Cocur\Slugify\Slugify;
use RadekZ\Blog\BackendBundle\Entity\Account;
use RadekZ\Blog\BackendBundle\Entity\Post;
use RadekZ\Blog\BackendBundle\Entity\Tag;
use RadekZ\Blog\BackendBundle\EventDispatcher\PostCreateBeforeEvent;
use RadekZ\Blog\BackendBundle\EventDispatcher\PostUpdateBeforeEvent;
use RadekZ\Blog\BackendBundle\Events;
use RadekZ\Blog\BackendBundle\Form\Type\Post\CreateType;
use RadekZ\Blog\BackendBundle\Repository\PostRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Form\ClickableInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Doctrine\Common\Persistence\ManagerRegistry;
use Swift_Mailer;
use RadekZ\Blog\BackendBundle\EventDispatcher\PostReadEvent;
use RadekZ\Blog\BackendBundle\EventData\RedirectToRoute;
use RadekZ\Blog\BackendBundle\EventDispatcher\PostCreateUpdateEvent;

class PostSubscriber implements EventSubscriberInterface
{
    protected $mailer;
    protected $translator;
    protected $urlGenerator;
    protected $tokenStorage;
    protected $sender;

    /**
     * @var ManagerRegistry
     */
    protected $managerRegistry;

    public function __construct(Swift_Mailer $mailer, UrlGeneratorInterface $urlGenerator,
                                TranslatorInterface $translator,
                                ManagerRegistry $managerRegistry,
                                TokenStorageInterface $tokenStorage,
                                string $sender
    )
    {
        $this->mailer = $mailer;
        $this->urlGenerator = $urlGenerator;
        $this->translator = $translator;
        $this->managerRegistry = $managerRegistry;
        $this->tokenStorage = $tokenStorage;
        $this->sender = $sender;

    }

    public static function getSubscribedEvents(): array
    {
        return [
            //Events::POST_READ => 'onPostRead',
            Events::POSTS_SEARCHED_BY_TAG_BEFORE => 'onPostsSearchedByTagBefore',
            Events::POST_CREATE_BEFORE => 'onPostCreateBefore',
            Events::POST_UPDATE_BEFORE => 'onPostUpdateBefore',
            Events::POST_READ_ESI => 'onPostReadESI',
        ];
    }

    public function onPostRead(PostReadEvent $event): void
    {
        $post = $event->getSubject();
        $post->setViewReadNumber($post->getViewReadNumber() + 1);
        $manager = $this->managerRegistry->getManager();
        $manager->persist($post);
        $manager->flush();
    }

    public function onPostReadESI(PostReadEvent $event): void
    {
        $manager = $this->managerRegistry->getManager();

        $post = $event->getSubject();
        if(!$post) {

            return;
        }

        $token = $this->tokenStorage->getToken();
        $doIncreaseViewCounter = true;
        if($token) {
            /** @var Account|string $account */
            $account = $token->getUser();
            if(is_object($account)) {
                $author = $post->getAuthor();
                if($author) {
                    if($author->getId() == $account->getId()) {
                        $doIncreaseViewCounter = false;
                    }
                }
                if($doIncreaseViewCounter) {
                    $modifier = $post->getModifier();
                    if($modifier) {
                        if($modifier->getId() == $account->getId()) {
                            $doIncreaseViewCounter = false;
                        }
                    }
                }
            }
        }
        if(!$doIncreaseViewCounter) {

            return ;
        }
        $post->setViewReadNumber($post->getViewReadNumber() + 1);

        $manager->persist($post);
        $manager->flush();
    }

    public function onPostsSearchedByTagBefore(GenericEvent $event): void
    {
        /** @var Tag $tag */
        $tag = $event->getSubject();
        $tag->setSearchedByTagNumber($tag->getSearchedByTagNumber() + 1);
        $manager = $this->managerRegistry->getManager();
        $manager->persist($tag);
        $manager->flush();
    }

    public function onPostCreateBefore(PostCreateBeforeEvent $event): void
    {
        $post = $event->getSubject();
        $account = $event->getAccount();
        $form = $event->getForm();

        $slugify = new Slugify(['lowercase' => false]);
        $post->setSlug($slugify->slugify($post->getTitle()));

        /** @var PostRepository $postRepo */
        $postRepo = $this->managerRegistry->getRepository(Post::class);

        $slugCounter = 0;
        while($postX = $postRepo->findOneBySlug($post->getSlug() . Post::generateSlugSuffix($slugCounter))) {
            $slugCounter++;
        }
        $post->setSlug($post->getSlug() . Post::generateSlugSuffix($slugCounter));

        $isPublished = $this->isSubmittedPublished($form);
        $event->setIsActionPublished($isPublished);

        $post->setStatus(Post::STATUS_NEW);
        if($isPublished) {
            $post->setStatus(Post::STATUS_PUBLISHED);
        }
    }

    public function onPostUpdateBefore(PostUpdateBeforeEvent $event)
    {
        $post = $event->getSubject();
        $account = $event->getAccount();
        $form = $event->getForm();
        $date = $event->getDate();

        $post->setModified($date);
        $post->setModifier($account);
        $post->setModifierNickName($account->getNickname());

        $postRepo = $this->managerRegistry->getRepository(Post::class);

        $oldPostTitle = $event->getOldPostTitle();
        if($oldPostTitle !== $post->getTitle()) {
            $slugify = new Slugify(['lowercase' => false]);
            $post->setSlug($slugify->slugify($post->getTitle()));

            $slugCounter = 0;
            while($postX = $postRepo->findOneBySlug($post->getSlug() . Post::generateSlugSuffix($slugCounter))) {
                $slugCounter++;
            }
            $post->setSlug($post->getSlug() . Post::generateSlugSuffix($slugCounter));
        }

        $isPublished = $this->isSubmittedPublished($form);
        $event->setIsActionPublished($isPublished);

        if($post->getStatus() > Post::STATUS_NEW) {
            $post->setStatus(Post::STATUS_SAVED_DRAFT);
        }

        if($isPublished) {
            if($post->getStatus() < Post::STATUS_NEW + 1) {
                $post->setPublishedAt($date);
            }
            $post->setStatus(Post::STATUS_PUBLISHED);
        }
    }

    protected function isSubmittedPublished(FormInterface $form)
    {
        /** @var ClickableInterface $submitButton */
        $submitButton = $form->get(CreateType::SUBMIT_SAVE_AND_PUBLISH);
        if($submitButton->isClicked()) {

            return true;
        }

        return false;
    }
}
