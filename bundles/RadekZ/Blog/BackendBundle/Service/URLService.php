<?php
declare(strict_types=1);

namespace RadekZ\Blog\BackendBundle\Service;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class URLService
{
    public const HOMEPAGE_PATH_NAME = "homepage";
    public const URL_STACK_SESSION_KEY = "_page-url-stack";

    use TargetPathTrait;

    /**
     * @var RequestStack
     */
    protected $requestStack;
    /**
     * @var RouterInterface
     */
    protected $router;

    public function __construct(RequestStack $requestStack, RouterInterface $router)
    {
    	$this->requestStack = $requestStack;
    	$this->router = $router;
    }

    public function getPathURL(string $target = "main", string $fallbackPathName = self::HOMEPAGE_PATH_NAME): string
    {
        $url = $this->getTargetPath($this->requestStack->getCurrentRequest()->getSession(), $target);
        if(!$url) {
            $url = $this->router->generate($fallbackPathName);
        }

        return $url;
    }

    public function getPreviousPageURLByRequest(Request $request): ?string
    {
    	$session = $request->getSession();

        $prevURL = null;

    	$pageURLStack = $session->get(self::URL_STACK_SESSION_KEY);
    	if(!$pageURLStack) {
            //$pageURLStack = [];

            return $prevURL;
        }

        $prevIndex = count($pageURLStack) - 2;
        if(isset($pageURLStack[$prevIndex])) {
            $prevURL = $pageURLStack[$prevIndex];
        }

    	return $prevURL;
    }

    public function addPageURLByRequest(Request $request): void
    {
        if(!$request->isMethod(Request::METHOD_GET)) {

            return ;
        }
        if($request->isXmlHttpRequest()){

            return ;
        }

        $requestedURI = $request->getRequestUri();
        $assetsURIs = [
            '/js/',
            '/css/',
            '/bundles/',
            '/build/',
            '/images/',
            '/fonts/',
            '/_'
        ];
        //$isHTMLRequest = true;
        foreach ($assetsURIs as $assetBaseURI) {
            if($assetBaseURI) {
                if(strpos($requestedURI, $assetBaseURI) === 0) {
                    //$isHTMLRequest = false;

                    return ;
                }
            }
        }

        $session = $request->getSession();

        $pageURLStack = $session->get(self::URL_STACK_SESSION_KEY);
        if(!$pageURLStack) {
            $pageURLStack = [];
        }

        //$currentURL = $request->getBaseUrl() . $request->getRequestUri();
        $currentURL = $request->getRequestUri();

        $count = count($pageURLStack);
        if($count > 0) {
            $lastItemKey = $count - 1;
            $lastURL = $pageURLStack[$lastItemKey];
            if($lastURL === $currentURL) {
                //var_dump($pageURLStack). "\n";

                return ;
            }
            $prevXItemKey = $count - 2;
            if(isset($pageURLStack[$prevXItemKey])) {
                if($pageURLStack[$prevXItemKey] === $currentURL) {
                    array_pop($pageURLStack);

                    $session->set(self::URL_STACK_SESSION_KEY, $pageURLStack);

                    //var_dump($pageURLStack). "\n";

                    return ;
                }
            }
        }

        $pageURLStack[] = $currentURL;
        $count = count($pageURLStack);
        $maxCount = 10;
        if($count > $maxCount) {
            for($i = 1; $i <= ($count - $maxCount); $i++) {
                $k = $i - 1;
                unset($pageURLStack[$k]);
            }

            $pageURLStack = array_values($pageURLStack);
        }
        $session->set(self::URL_STACK_SESSION_KEY, $pageURLStack);
        //var_dump($pageURLStack). "\n";
    }

    public function getPreviousPagePath(string $fallbackRoutName = self::HOMEPAGE_PATH_NAME, array $params = [])
    {
        $url = $this->getPreviousPageURLByRequest($this->requestStack->getCurrentRequest());
        if(!$url) {
            if($fallbackRoutName) {
                $url = $this->router->generate($fallbackRoutName, $params);
            }
        }

        return $url;
    }

}

