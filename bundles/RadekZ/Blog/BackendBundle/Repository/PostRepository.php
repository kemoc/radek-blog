<?php
declare(strict_types=1);


namespace RadekZ\Blog\BackendBundle\Repository;

use RadekZ\Blog\BackendBundle\Entity\Account;
use RadekZ\Blog\BackendBundle\Entity\Post;
use RadekZ\Blog\BackendBundle\Entity\Tag;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\Query;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
//use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use DateTime;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Radek Z
 */
class PostRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Post::class);
    }

    public function findLatest(int $page = 1, Tag $tag = null): Pagerfanta
    {
        $qb = $this->createQueryBuilder('p')
            ->andWhere('p.status > :status')
            ->setParameter('status', $this->getPostStatusDeleted(), Type::INTEGER)
            ->orderBy('p.publishedAt', 'DESC')
        ;

        if (null !== $tag) {
            $qb->andWhere('p.tags = :tag')
                ->setParameter('tag', $tag);
        }

        return $this->createPaginator($qb->getQuery(), $page);
    }

    public function findLatestByAuthor(Account $author, int $page = 1, Tag $tag = null): Pagerfanta
    {
        $qb = $this->createQueryBuilder('p')
            ->andWhere('p.status > :status')
            ->setParameter('status', $this->getPostStatusDeleted(), Type::INTEGER)
            ->andWhere('p.author = :author')
                ->setParameter('author', $author)
            ->orderBy('p.publishedAt', 'DESC')
        ;

//        $qb = $this->createQueryBuilder('p')
//            ->select('a', 't')
//            ->innerJoin('p.author', 'a')
//            ->where(':author MEMBER OF p.author')
//            ->leftJoin('p.tags', 't')
//            ->andWhere('p.publishedAt <= :now')
//            ->orderBy('p.publishedAt', 'DESC')
//            ->setParameter('now', new \DateTime());

        if (null !== $tag) {
            $qb->andWhere('p.tag = :tag')
                ->setParameter('tag', $tag);
        }
//        $qb->setParameter('author', $author);

        return $this->createPaginator($qb->getQuery(), $page);
    }
    public function findLatestByAuthorInMonth(Account $author, DateTime $date, int $page = 1, Tag $tag = null
    ): Pagerfanta
    {
        $qb = $this->createQueryBuilder('p')
            ->andWhere('p.status > :status')
            ->setParameter('status', $this->getPostStatusDeleted(), Type::INTEGER)
            ->andWhere('p.author = :author')
            ->setParameter('author', $author)
            ->andWhere('p.publishedAt LIKE :date')
            ->setParameter('date', $date->format('Y-m') . '%', Type::STRING)
            ->orderBy('p.publishedAt', 'DESC')
        ;
        if (null !== $tag) {
            $qb->andWhere('p.tag = :tag')
                ->setParameter('tag', $tag);
        }

        return $this->createPaginator($qb->getQuery(), $page);
    }

    public function findLatestUnpublished(int $page = 1, Tag $tag = null, int $maxPerPage = Post::NUM_ITEMS
    ): Pagerfanta
    {
        $qb = $this->createQueryBuilder('p')
            ->andWhere('p.status < :status')
            ->setParameter('status', $this->getPostStatusDeleted(), Type::INTEGER)
            ->orderBy('p.publishedAt', 'DESC')
        ;

        if (null !== $tag) {
            $qb->andWhere('p.tags = :tag')
                ->setParameter('tag', $tag);
        }

        return $this->createPaginator($qb->getQuery(), $page, $maxPerPage);
    }

    public function findMostPopular(int $page = 1): Pagerfanta
    {
        $qb = $this->createQueryBuilder('p')
            ->andWhere('p.status > :status')
            ->setParameter('status', $this->getPostStatusDeleted(), Type::INTEGER)
            ->andWhere('p.publishedAt <= :now')
            ->setParameter('now', new \DateTime())
            ->orderBy('p.viewReadNumber', 'DESC')
            ->addOrderBy('p.publishedAt', 'DESC')
            ;

        return $this->createPaginator($qb->getQuery(), $page);
    }

    public function findMostPopularByAuthor(Account $author, int $page = 1): Pagerfanta
    {
        $qb = $this->createQueryBuilder('p')
            ->andWhere('p.status > :status')
            ->setParameter('status', $this->getPostStatusDeleted(), Type::INTEGER)
            ->orderBy('p.viewReadNumber', 'DESC')
            ->addOrderBy('p.publishedAt', 'DESC')
        ;
        $qb->andWhere('p.author = :author')
            ->setParameter('author', $author);

        return $this->createPaginator($qb->getQuery(), $page);
    }

    public function findMostPopularInMonth(\DateTime $month, int $page = Post::NUM_ITEMS)
    {
        $qb = $this->createQueryBuilder('p')
            ->andWhere('p.status > :status')
            ->setParameter('status', $this->getPostStatusDeleted(), Type::INTEGER)
            ->andWhere('p.publishedAt <= :now')
            ->setParameter('now', new \DateTime())
            ->andWhere('p.publishedAt LIKE :month')
            ->setParameter('month', $month->format("Y-m") . "%", Type::STRING)
            ->orderBy('p.viewReadNumber', 'DESC')
            ->addOrderBy('p.publishedAt', 'DESC')
        ;

        return $this->createPaginator($qb->getQuery(), $page);
    }

    public function findMostPopularInMonthByAuthor(Account $author, \DateTime $month, int $page = Post::NUM_ITEMS)
    {
        $qb = $this->createQueryBuilder('p')
            ->andWhere('p.status > :status')
            ->setParameter('status', $this->getPostStatusDeleted(), Type::INTEGER)
            ->andWhere('p.publishedAt LIKE :month')
            ->setParameter('month', $month->format("Y-m") . "%", Type::STRING)
            ->orderBy('p.viewReadNumber', 'DESC')
            ->addOrderBy('p.publishedAt', 'DESC')
        ;
        $qb->andWhere('p.author = :author')
            ->setParameter('author', $author);

        return $this->createPaginator($qb->getQuery(), $page);
    }

    private function createPaginator(Query $query, int $page, int $maxPerPage = Post::NUM_ITEMS): Pagerfanta
    {
        $paginator = new Pagerfanta(new DoctrineORMAdapter($query));
        $paginator->setMaxPerPage($maxPerPage);
        $paginator->setCurrentPage($page);

        return $paginator;
    }

    public function findBySearchQuery(string $rawQuery, int $limit = Post::NUM_ITEMS): array
    {
        $query = $this->sanitizeSearchQuery($rawQuery);
        $searchTerms = $this->extractSearchTerms($query);

        if (0 === count($searchTerms)) {
            return [];
        }

        $queryBuilder = $this->createQueryBuilder('p');

        foreach ($searchTerms as $key => $term) {
            $queryBuilder
                ->orWhere('p.title LIKE :t_'.$key)
                ->setParameter('t_'.$key, '%'.$term.'%', Type::STRING)
            ;
        }
        /** @var Post[] $res */
        $res = $queryBuilder
            ->andWhere('p.status > :status')
            ->setParameter('status', $this->getPostStatusDeleted(), Type::INTEGER)
            ->orderBy('p.publishedAt', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();

        return $res;
    }

    /**
     * Removes all non-alphanumeric characters except whitespaces.
     */
    private function sanitizeSearchQuery(string $query): string
    {
        return trim(preg_replace('/[[:space:]]+/', ' ', $query));
    }

    /**
     * Splits the search query into terms and removes the ones which are irrelevant.
     */
    private function extractSearchTerms(string $searchQuery): array
    {
        $terms = array_unique(explode(' ', $searchQuery));

        return array_filter($terms, function ($term) {
            return 1 < mb_strlen($term);
        });
    }

    public function findOneBySlug(string $slug): ?Post
    {
        /** @var null|Post $res */
    	$res = $this->findOneBy([
    	    'slug' => $slug
        ]);

    	return $res;
    }
    public function findOnePublishedBySlug(string $slug): ?Post
    {
        $qb = $this->createQueryBuilder('p')
            ->andWhere('p.status > :status')
            ->setParameter('status', $this->getPostStatusDeleted(), Type::INTEGER)
            ->andWhere('p.slug = :slug')
            ->setParameter('slug', $slug, Type::STRING)
        ;
        /** @var Post[] $resArr */
        $resArr = $qb->getQuery()->getResult();
        if($resArr) {

            return $resArr[0];
        }

        return null;
    }

    public function findOnePublished(int $id): ?Post
    {
        $qb = $this->createQueryBuilder('p')
            ->andWhere('p.status > :status')
            ->setParameter('status', $this->getPostStatusDeleted(), Type::INTEGER)
            ->andWhere('p.id = :id')
            ->setParameter('id', $id, Type::INTEGER)
        ;
        /** @var Post[] $resArr */
        $resArr = $qb->getQuery()->getResult();
        if($resArr) {

            return $resArr[0];
        }

        return null;
    }
    public function findOneUnpublished(int $id): ?Post
    {
        $qb = $this->createQueryBuilder('p')
            ->andWhere('p.status < :status')
            ->setParameter('status', $this->getPostStatusDeleted(), Type::INTEGER)
            ->andWhere('p.id = :id')
            ->setParameter('id', $id, Type::INTEGER)
        ;
        /** @var Post[] $resArr */
        $resArr = $qb->getQuery()->getResult();
        if($resArr) {

            return $resArr[0];
        }

        return null;
    }

    public function findOnePublishedBySlugAndAuthor(string $slug, Account $author): ?Post
    {
        $qb = $this->createQueryBuilder('p')
            ->andWhere('p.status > :status')
            ->setParameter('status', $this->getPostStatusDeleted(), Type::INTEGER)
            ->andWhere('p.slug = :slug')
            ->setParameter('slug', $slug, Type::STRING)
            ->andWhere('p.author = :author')
            ->setParameter('author', $author)
        ;
        /** @var Post[] $resArr */
        $resArr = $qb->getQuery()->getResult();
        if($resArr) {

            return $resArr[0];
        }

        return null;
    }
    public function findOneBySlugAndAuthor(string $slug, Account $author): ?Post
    {
        $qb = $this->createQueryBuilder('p')
            ->andWhere('p.status != :status')
            ->setParameter('status', $this->getPostStatusDeleted(), Type::INTEGER)
            ->andWhere('p.slug = :slug')
            ->setParameter('slug', $slug, Type::STRING)
            ->andWhere('p.author = :author')
            ->setParameter('author', $author)
        ;
        /** @var Post[] $resArr */
        $resArr = $qb->getQuery()->getResult();
        if($resArr) {

            return $resArr[0];
        }

        return null;
    }
    public function findOneUnpublishedBySlugAndAuthor(string $slug, Account $author): ?Post
    {
        $qb = $this->createQueryBuilder('p')
            ->andWhere('p.status < :status')
            ->setParameter('status', $this->getPostStatusDeleted(), Type::INTEGER)
            ->andWhere('p.slug = :slug')
            ->setParameter('slug', $slug, Type::STRING)
            ->andWhere('p.author = :author')
            ->setParameter('author', $author)
        ;
        /** @var Post[] $resArr */
        $resArr = $qb->getQuery()->getResult();
        if($resArr) {

            return $resArr[0];
        }

        return null;
    }

    public function getPostStatusDeleted()
    {

    	return Post::STATUS_DELETED;
    }
}
