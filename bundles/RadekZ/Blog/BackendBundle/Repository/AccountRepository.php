<?php
declare(strict_types=1);

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace RadekZ\Blog\BackendBundle\Repository;

use RadekZ\Blog\BackendBundle\Entity\Account;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * This custom Doctrine repository is empty because so far we don't need any custom
 * method to query for application user information. But it's always a good practice
 * to define a custom repository that will be used when the application grows.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Ryan Weaver <weaverryan@gmail.com>
 * @author Javier Eguiluz <javier.eguiluz@gmail.com>
 */
class AccountRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Account::class);
    }

    /**
     * @param string $username
     * @return null|Account
     */
    public function findByUsername(string $username): ?Account
    {
        /** @var null|Account $ret */
        $ret = $this->findOneBy([
            'username' => $username
        ]);
        return $ret;
    }

    /**
     * @param string $username
     * @param string $password
     * @return null|Account
     */
    public function findByUsernameAndPassword(string $username, string $password): ?Account
    {
        /** @var null|Account $ret */
        $ret = $this->findOneBy([
            'username' => $username,
            'password' => $password
        ]);

        return $ret;
    }
}
